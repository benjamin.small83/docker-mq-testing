import pika
import time

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq.sys'))
channel = connection.channel()
channel.queue_declare(queue="hello")

print("Producing a message", flush=True)
channel.basic_publish(exchange='',
                      routing_key='hello',
                      body='Hello World!')