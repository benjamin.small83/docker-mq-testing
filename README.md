A basic docker compose and python scripts for producing and consuming messages in the "hello" queue.

Consumer/Producer needs to sleep for 15 seconds before connecting - we need rabbitmq to start. The container detaches before rabbitmq is completely started, so depends-on doesn't really work.

`docker-compose up -d` to start the systems

`docker-compose exec producer.sys /bin/sh` to attach to the producer to generate a message. Once in the image use `python /main.py` to generate a message in the queue
